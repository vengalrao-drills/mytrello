import React, { useState } from 'react'
import useStyles from '../Styles/styles'
import AddIcon from '@mui/icons-material/Add';
import { Typography } from '@mui/material';
import { Box } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import DialogButton from '../MuiUsable/DialogButton';
import CloseIcon from '@mui/icons-material/Close';
import { Button } from '@mui/material';
import exportData from '../Data/key';
import axios from 'axios';
import DeleteOption from '../MuiUsable/deleteOption';
import Toast from '../MuiUsable/Toast';

const BoardsDisplay = ({ PostBoardData, data, handleChange ,setData }) => {
    const classes = useStyles();
    const navigate = useNavigate();
    const gotTo = (id, board) => {
        navigate(`/boards/${id}`, { state: board.name })
    }
    let [error, setError] = useState('')

    let { key, APIToken } = exportData
    const deleteIt = (id) => {
        axios.delete(`https://api.trello.com/1/boards/${id}?key=${key}&token=${APIToken}`).then(response => {
            let value = data.filter(val=>val.id!= id)
            setData( value  )            
        }).catch((e) => {
            setError('Error 401 Bad Resquest - Board Not Deleted ');
        })
    }

    let boards = null;
    if (data) {
        boards = data.map((board) => {
            return (
                <Box sx={{ display: "flex", flexDirection: "row", alignItems: "baseline", "&:hover .CloseIcon": { opacity: "1" } }} key={board.id} className={classes.Allboards} >
                    <Typography sx={{ height: "100%" }} className={classes.boardsNames} variant="h4" onClick={() => gotTo(board.id, board)} >
                        {board.name}
                    </Typography>
                    <Button sx={{ opacity: "1", color: "black", transition: "0.3s", '&:hover': { background: "white" } }}  >
                        <DeleteOption sx={{ margin: "0", padding: "0" }} deleteIt={deleteIt} id={board.id}     >
                            <CloseIcon sx={{ color: "black" }} className='CloseIcon' />
                        </DeleteOption>
                    </Button>
                    {/* {error.length >0 ?  */}
                    <Toast error={error} setError={setError} /> 
                {/* } */}
                </Box>
            );
        });
    }


    return (
        <Box className={classes.boardsContainer}>
            {boards}
            <Box className={`${classes.Allboards} ${classes.l} `}   >
                <DialogButton name='boardName' handleChange={handleChange} PostBoardData={PostBoardData} />
            </Box>
        </Box>
    )
}

export default BoardsDisplay