import React, { useEffect, useState } from 'react'
import axios from 'axios';
import exportData from '../Data/key';
import { Card, Typography } from '@mui/material';
import { CardContent } from '@mui/material';
import { Button } from '@mui/material';
import { Box } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import CheckListsDisplay from '../MuiUsable/CheckListsDisplay';
import AddCard from '../MuiUsable/AddCard';
import Toast from '../MuiUsable/Toast';

const CardData = ({ id  }) => {
    const { key, APIToken } = exportData;
    let [cardsData, setCardsData] = useState([]);
    const [error , setError] = useState('')
    const getCards = async () => { 
        console.log('getcards function called')
        await axios
            .get(`https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${APIToken}`)
            .then((response) => { 
                setCardsData(response.data);
            })
            .catch((e) => {
                console.log(e);
                setError('404 Error - Cards not Fetched')
            });
    }
    useEffect(()=>{
        getCards()
    },[])
    

    const deleteCard = (ID) => {
          axios.delete(`https://api.trello.com/1/cards/${ID}?key=${key}&token=${APIToken}`).then(response => {
          let answer =  cardsData.filter(card => card.id !== ID)
          setCardsData(answer); 
        }).catch((e) => {
            console.log(e)
            setError('404 Error -  Card Not Deleted')
        })
    }

    let cardDataDisplay = '';
    if (cardsData.length > 0) {
        cardDataDisplay = cardsData.map((card) => {
            // console.log('card ', card)
            return (
                <Box key={card.id}   >
                    <Card sx={{ margin: "0.9rem 0rem ", borderRadius: "0.5rem" }}  >
                        <CardContent className='{classes}' sx={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}  >
                            <CheckListsDisplay getCards={getCards} cardId={card.id} cardName={card.name}   >
                                <Typography variant='h6' sx={{ }} onClick={()=>getCards()} >{card.name}   </Typography>
                            </CheckListsDisplay>
                            <Button onClick={() => { deleteCard(card.id) }}  >  <DeleteIcon sx={{ color: "black" }} />     </Button>
                        </CardContent>
                    </Card>
                </Box>
            )
        });
    }

    return (
        <div>
            {cardDataDisplay}
            <AddCard text={'Enter Card Name'} setCardsData={setCardsData}  id={id}    /> 
            {  error.length>0 ?  <Toast  error={error} setError={setError} /> :"" }      
        </div>
    )
}

export default CardData  