import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import { Box, Card } from '@mui/material';
import axios from 'axios';
import exportData from '../Data/key';
import AddCheckList from './AddCheckList';
import CheckItems from '../Components/CheckItems';
import Toast from './Toast';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

export default function CheckListsDisplay({ children, cardId, cardName, getCards }) {
  const [open, setOpen] = React.useState(false);

  let { key, APIToken } = exportData;
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [error , setError] =useState('')

  let [checkListData, setCheckListData] = useState([]);
  const getCheckLists = () => {
    axios.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${APIToken}`).then(response => {
      setCheckListData(response.data)
    })
      .catch((e) => {
        console.log(e);
        setError('404 Error - Checklists Data Not found')
      })
  }

  const createCheckList = async (name) => {
    await axios.post(`ttps://api.trello.com/1/cards/${cardId}/checklists?name=${name}&key=${key}&token=${APIToken}`).then(response => {
      setCheckListData(prev=>[...prev , response.data])
    }).catch((e) => {
      console.log(e)
      setError('404 Error - Cannot Create CheckList');
    })
  }

  const deleteCheckList = (cardId, idCheckItem) => {
    console.log( idCheckItem , checkListData ) 
    axios.delete(`https://api.trello.com/1/cards/${cardId}/checklists/${idCheckItem}?key=${key}&token=${APIToken}`).then(response => {
    setCheckListData(prev=>prev.filter(p=>p.id!=idCheckItem))
    }).catch(e => {
      console.log(e);
      setError('404 Error - Cannot Delete Checklist');
    })
  }

  useEffect(() => {
    getCheckLists()
  }, [])

  return (
    <React.Fragment>
      <Box variant="outlined" sx={{ width:"100%" , cursor:"pointer"  }} onClick={() => { getCheckLists(); handleClickOpen() }}>
        {children} 
      </Box>

      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle sx={{ m: 1, p: 0, display: "flex", flexDirection: "row", alignItems: "center", width: 'lg' }} id="customized-dialog-title">
          <Typography variant='h3' sx={{ padding: "0.5rem 2.5rem", minWidth: "35rem", textAlign: "center" }} > {cardName}   </Typography>
        </DialogTitle>

        <AddCheckList createCheckList={createCheckList} />
        {  error.length>0 ?  <Toast  error={error} setError={setError} /> :"" }      

        <DialogContent dividers>
          {checkListData.map((work, index) => {
            return (
              <Card sx={{ margin: "0rem 0.5rem 2rem 0.5rem", }} key={work.id}  >
                <Box sx={{ padding: "0.5rem 0.5rem", display: "flex", justifyContent: "space-between", }} >
                  <Typography variant='h4' key={index}>
                    {work.name}  
                  </Typography>
                  <Button  onClick={() => {deleteCheckList(cardId, work.id) }}  sx={{display:"flex" , alignItems:"center"  }}>
                  <Typography  variant='h6' >DELETE</Typography> <CloseIcon sx={{color:"red"}} />
                  </Button>
                </Box>
                <Box>
                  <CheckItems checkListData={checkListData} setCheckListData={setCheckListData}  getCheckLists={getCheckLists}  work={work  } />   
                </Box>
              </Card>
            )
          })}
        </DialogContent>

        <DialogActions sx={{ display: 'flex', justifyContent: "flex-end" }}>
          <Button autoFocus onClick={handleClose}>
            <Typography>Close </Typography>
          </Button>
        </DialogActions>

      </BootstrapDialog>
    </React.Fragment>
  );
}









