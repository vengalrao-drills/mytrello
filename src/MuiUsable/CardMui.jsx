import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import useStyles from '../Styles/styles';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';

import axios from 'axios';
import CardData from '../Components/CardData';
import AddAnotherList from './AddAnotherList';
import exportData from '../Data/key';
import DeleteOption from './deleteOption';
import { useParams } from 'react-router-dom';
import AddCard from './AddCard'; 
import Toast from './Toast';

export default function CardMui({ lists  , setLists }) { 
  const classes = useStyles();
  const { key, APIToken } = exportData;  
  const [error , setError] = React.useState('')


  const deleteIt =   (id) => {
      axios.put(`ttps://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${APIToken}`).then(response => {
      setLists(lists.filter(list=>list.id!=id));
    }).catch(e => {
      setError('404 error -  Cannot Delete List')
      console.log(e);
    })
  }
  let params = useParams();
  let currentBoardId = params.id;
  let allLists = '';

  if (lists[0]) {
    allLists = lists.map((list) => (
      <Card key={list.id} variant="outlined" sx={{ background: '#F1F2F4', alignSelf: 'flex-start', marginRight: "1rem",  minWidth:"20rem"   }} className={classes.cardList}>
        <Typography gutterBottom variant="h5" sx={{ margin: "0 0 1rem 0", display: "flex", flexDirection: "row", justifyContent: "space-between" }} component="div">
          {list.name} 
          <DeleteOption deleteIt={deleteIt} id={list.id} >
            <MoreHorizIcon sx={{ textAlign: "right", cursor: "pointer" }} /> 
          </DeleteOption>
        </Typography>
        <CardData id={list.id}  />
      </Card>
    ))
  }

  let addAnotherList = <Box variant="outlined" sx={{ background: '#e8e6e6', color: "black", alignSelf: 'flex-start', minWidth: "20rem",     }} className={classes.cardList} >
    <Typography gutterBottom variant="h5" component="div">
      <AddAnotherList  setLists={setLists} id={currentBoardId} />
    </Typography>
  </Box>

  return (

    <Box sx={{ display: 'flex', overflowX: "auto" }}>
      {allLists}
      {addAnotherList}
      {  error.length>0 ?  <Toast  error={error} setError={setError} /> :"" }      
    </Box>
  );
}









 