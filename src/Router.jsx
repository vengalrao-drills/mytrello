import React from 'react'
import { Routes , Route  } from 'react-router-dom'       
import App from './App'
import Lists from './Components/Lists'
const Router = () => {
  return (
    <div>
      <Routes>
        <Route  path='/'   element={<App/> } />  
        <Route  path ='boards/:id' element= {<Lists/>} />
      </Routes>
    </div>
  )
}

export default Router
